<?php

declare(strict_types=1);

namespace Drupal\Tests\views_st\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\UserInterface;
use Drupal\views\Entity\View;
use Drupal\views\ViewEntityInterface;

/**
 * @group views_st
 */
class ViewStAdminContentTest extends WebDriverTestBase {

  protected false|UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'views',
    'views_ui',
    'views_st',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    try {
      $this->getSession()->getDriver()->maximizeWindow();
    }
    catch (\Throwable) {
      // Do nothing.
    }

    $this->adminUser = $this->drupalCreateUser([
      // Node.
      'administer nodes',
      'access content overview',
      // System.
      'access administration pages',
      'access content',
      'administer themes',
      'view the administration theme',
      // Views Secondary Tabs.
      'views_st.views_st.admin',
      // Views UI.
      'administer views',
    ]);
    $this->drupalLogin($this->adminUser);

    $this
      ->installTheme('claro')
      ->setAdminTheme('claro');
  }

  protected function getBaseUrlPath(): string {
    return rtrim(
      (string) parse_url($this->baseUrl, \PHP_URL_PATH),
      '/',
    );
  }

  /**
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\DriverException
   */
  public function testAdminContent(): void {
    $stParentId = 'system.admin_content';
    $stWeight = 0;

    $this->createViewsSecondaryTab([
      'id' => 'admin_content_node_all',
      'parent[id]' => $stParentId,
      'weight' => $stWeight,
      'view[id_and_display]' => 'content:page_1',
    ]);

    foreach (['article', 'news'] as $nodeTypeId) {
      $this->drupalCreateContentType(['type' => $nodeTypeId]);
      $this->createViewWithPageDisplay(
        'node',
        $nodeTypeId,
        [
          'id' => "admin_content_node_$nodeTypeId",
        ],
      );
      $this->createViewsSecondaryTab([
        'id' => "admin_content_node_$nodeTypeId",
        'parent[id]' => $stParentId,
        'weight' => ++$stWeight,
        'view[id_and_display]' => "admin_content_node_$nodeTypeId:page_1",
      ]);
    }

    $this->drupalGet('admin/content');
    $page = $this->getSession()->getPage();
    $secondaryTabElements = $page->findAll('css', '.tabs.tabs--secondary > .tabs__tab > a.tabs__link');

    static::assertCount(
      3,
      $secondaryTabElements,
      'There are three Secondary tabs.',
    );

    $baseUrlPath = $this->getBaseUrlPath();

    $expected = [
      [
        'text' => 'Content',
        'href' => "$baseUrlPath/admin/content",
      ],
      [
        'text' => 'article',
        'href' => "$baseUrlPath/admin/content/node/article",
      ],
      [
        'text' => 'news',
        'href' => "$baseUrlPath/admin/content/node/news",
      ],
    ];

    if (version_compare(\Drupal::VERSION, '10.3.0', '<')) {
      $expected[0]['text'] .= ' (active tab)';
    }

    foreach ($secondaryTabElements as $key => $secondaryTabElement) {
      static::assertSame($expected[$key]['text'], $secondaryTabElement->getText());
      static::assertSame($expected[$key]['href'], $secondaryTabElement->getAttribute('href'));
    }
  }

  /**
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\DriverException
   */
  protected function createViewWithPageDisplay(
    string $entityTypeId,
    string $bundleId,
    array $values,
  ): static {
    $wizardKey = $entityTypeId;
    $pagePath = match ($entityTypeId) {
      'node' => "/admin/content/$entityTypeId/$bundleId",
      default => NULL,
    };

    $formValues = [
      'label' => $bundleId,
      'id' => $values['id'],
      'show[wizard_key]' => $wizardKey,
      'show[type]' => $bundleId,
      'page[create]' => TRUE,
      'page[title]' => $bundleId,
      'page[path]' => $pagePath,
      'page[style][style_plugin]' => 'default',
      'page[style][row_plugin]' => 'titles',
    ];

    $this->drupalGet('admin/structure/views/add');

    $this->fillLabelAndMachineName(
      [
        'locator' => 'label',
        'value' => $formValues['label'],
      ],
      [
        'locator' => 'id',
        'value' => $formValues['id'],
      ],
    );

    $this->getSession()->getPage()->fillField('show[wizard_key]', $formValues['show[wizard_key]']);
    // @todo Wait for AJAX to finish.
    sleep(2);
    $this->getSession()->getPage()->fillField('show[type]', $formValues['show[type]']);
    // @todo Wait for AJAX to finish.
    sleep(2);

    if (!empty($formValues['page[create]'])) {
      $this->getSession()->getPage()->checkField('page[create]');
      $this->assertSession()->waitForElementVisible('css', '*[name="page[create]"]');

      $this->getSession()->getPage()->fillField('page[title]', $formValues['page[title]']);
      $this->getSession()->getPage()->fillField('page[path]', $formValues['page[path]']);
      $this->getSession()->getPage()->fillField('page[style][style_plugin]', $formValues['page[style][style_plugin]']);
      // @todo Wait for AJAX to finish.
      sleep(2);
      $this->assertSession()->waitForElementVisible('css', '*[name="page[style][row_plugin]"]');
      $this->getSession()->getPage()->fillField('page[style][row_plugin]', $formValues['page[style][row_plugin]']);
      // @todo Wait for AJAX to finish.
      sleep(2);
    }

    $this->getSession()->getPage()->pressButton('Save and edit');

    $view = View::load($values['id']);
    static::assertInstanceOf(ViewEntityInterface::class, $view);

    return $this;
  }

  /**
   * Creates a new Secondary tab.
   *
   * Required keys:
   * - id
   * - parent[id]
   * - view[id_and_display]
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\DriverException
   */
  protected function createViewsSecondaryTab(array $values): static {
    $values += [
      'label' => $values['id'] ?? $this->randomMachineName(),
      'weight' => 1,
    ];

    $this->drupalGet('admin/structure/views_st');
    $this->getSession()->getPage()->clickLink('Add Views Secondary Tab');

    if (isset($values['label'])) {
      $this->fillLabelAndMachineName(
        [
          'locator' => 'label',
          'value' => $values['label'],
        ],
        isset($values['id'])
          ? [
            'locator' => 'id',
            'value' => $values['id'],
          ]
          : NULL,
      );
    }

    $page = $this->getSession()->getPage();
    if (isset($values['parent[id]'])) {
      $page->fillField('parent[id]', $values['parent[id]']);
    }

    if (isset($values['weight'])) {
      $page->fillField('weight', (string) $values['weight']);
    }

    if (isset($values['view[id_and_display]'])) {
      $page->fillField('view[id_and_display]', $values['view[id_and_display]']);
    }

    $page->pressButton('op');
    $this->assertSession()->pageTextContains('Views Secondary Tab has been created');

    return $this;
  }

  /**
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\DriverException
   */
  protected function fillLabelAndMachineName(
    array $label,
    ?array $machineName = NULL,
  ): static {
    $labelInput = $this
      ->getSession()
      ->getPage()
      ->findField($label['locator']);
    static::assertNotNull($labelInput);
    $labelInput->setValue($label['value']);
    $this->assertSession()->waitForElementVisible('css', '.machine-name-label');

    if (!$machineName) {
      return $this;
    }

    // @todo Find parent with CSS selector: ".form-item".
    $labelWrapper = $labelInput->getParent();
    $labelWrapper
      ->find('css', '*[data-drupal-selector="edit-id-machine-name-admin-link"]')
      ->press();

    $this
      ->getSession()
      ->getPage()
      ->fillField($machineName['locator'], $machineName['value']);

    return $this;
  }

  protected function installTheme(string $id): static {
    $this
      ->container
      ->get('theme_installer')
      ->install([$id]);

    return $this;
  }

  protected function setAdminTheme(string $id): static {
    $values = [
      'admin_theme' => $id,
      'use_admin_theme' => TRUE,
    ];
    $this->drupalGet('admin/appearance');
    $this->submitForm($values, 'Save configuration');

    return $this;
  }

}
