<?php

declare(strict_types=1);

namespace Drupal\Tests\views_st\Unit\SecondaryTab;

use Drupal\views_st\Entity\ViewsSt;
use Drupal\views_st\SecondaryTab\DefaultComparer;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * @group views_st
 *
 * @coversClass \Drupal\views_st\SecondaryTab\DefaultComparer
 */
#[CoversClass(DefaultComparer::class)]
class DefaultComparerTest extends TestCase {

  public static function casesCompare(): array {
    return [
      'same parent ASC' => [
        'expected' => ['b', 'c', 'a'],
        'options' => [],
        'items' => [
          'a' => [
            'parent.id' => 'system.a',
            'weight' => 3,
          ],
          'b' => [
            'parent.id' => 'system.a',
            'weight' => 1,
          ],
          'c' => [
            'parent.id' => 'system.a',
            'weight' => 2,
          ],
        ],
      ],
      'same parent DESC' => [
        'expected' => ['a', 'c', 'b'],
        'options' => [
          'ascending' => FALSE,
        ],
        'items' => [
          'a' => [
            'parent.id' => 'system.a',
            'weight' => 3,
          ],
          'b' => [
            'parent.id' => 'system.a',
            'weight' => 1,
          ],
          'c' => [
            'parent.id' => 'system.a',
            'weight' => 2,
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider casesCompare
   */
  #[DataProvider('casesCompare')]
  public function testCompare(array $expected, array $options, array $items): void {
    $entities = [];
    foreach ($items as $itemKey => $values) {
      $values['id'] = $itemKey;
      $entities[$itemKey] = new ViewsSt($values, 'views_st');
    }

    $comparer = new DefaultComparer();
    foreach ($options as $key => $value) {
      switch ($key) {
        case 'fieldNames':
          $comparer->setFieldNames($value);
          break;

        case 'ascending':
          $comparer->setAscending($value);
          break;
      }
    }

    uasort($entities, $comparer);

    static::assertSame($expected, array_keys($entities));
  }

}
