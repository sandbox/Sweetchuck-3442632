<?php

declare(strict_types=1);

namespace Drupal\views_st;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface ViewsStInterface extends ConfigEntityInterface {

  public function getParentId(): string;

  public function setParentId(string $id): static;

  public function getWeight(): int;

  public function setWeight(int $weight): static;

  public function getViewId(): string;

  public function setViewId(string $id): static;

  public function getViewDisplay(): string;

  public function setViewDisplay(string $id): static;

}
