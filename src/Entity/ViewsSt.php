<?php

declare(strict_types=1);

namespace Drupal\views_st\Entity;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\views_st\ViewsStInterface;

/**
 * @ConfigEntityType(
 *   id = "views_st",
 *   label            = @Translation("Views Secondary Tab"),
 *   label_collection = @Translation("Views Secondary Tabs"),
 *   label_singular   = @Translation("Views Secondary Tab"),
 *   label_plural     = @Translation("Views Secondary Tabs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Views Secondary Tab",
 *     plural   = "@count Views Secondary Tabs",
 *   ),
 *   entity_keys = {
 *     "id"     = "id",
 *     "label"  = "label",
 *     "weight" = "weight",
 *   },
 *   handlers = {
 *     "storage"      = "Drupal\views_st\SecondaryTab\Storage",
 *     "access"       = "Drupal\views_st\SecondaryTab\AccessControlHandler",
 *     "list_builder" = "Drupal\views_st\SecondaryTab\ListBuilder",
 *     "form" = {
 *       "default" = "Drupal\views_st\SecondaryTab\AddForm",
 *       "add"     = "Drupal\views_st\SecondaryTab\AddForm",
 *       "edit"    = "Drupal\views_st\SecondaryTab\EditForm",
 *       "delete"  = "Drupal\views_st\SecondaryTab\DeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\views_st\SecondaryTab\RouteProvider",
 *     }
 *   },
 *   links = {
 *     "add-form"    = "/admin/structure/views_st/add",
 *     "edit-form"   = "/admin/structure/views_st/manage/{views_st}",
 *     "delete-form" = "/admin/structure/views_st/manage/{views_st}/delete",
 *     "collection"  = "/admin/structure/views_st"
 *   },
 *   admin_permission = "views_st.views_st.admin",
 *   config_prefix = "views_st",
 *   config_export = {
 *     "id",
 *     "label",
 *     "parent",
 *     "weight",
 *     "view",
 *   },
 * )
 */
class ViewsSt extends ConfigEntityBase implements ViewsStInterface {

  /**
   * {@inheritdoc}
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    return \Drupal::getContainer()
      ->get('entity.views_st.comparer.default')
      ->compare($a, $b);
  }

  protected string $label = '';

  protected array $parent = [
    'id' => '',
    'parameters' => [],
  ];

  public function getParentId(): string {
    return $this->parent['id'];
  }

  public function setParentId(string $id): static {
    $this->parent['id'] = $id;

    return $this;
  }

  protected int $weight = 0;

  public function getWeight(): int {
    return $this->weight;
  }

  public function setWeight(int $weight): static {
    $this->weight = $weight;

    return $this;
  }

  protected array $view = [
    'id' => '',
    'display' => '',
    'arguments' => [],
  ];

  public function getViewId(): string {
    return $this->view['id'];
  }

  public function setViewId(string $id): static {
    $this->view['id'] = $id;

    return $this;
  }

  public function getViewDisplay(): string {
    return $this->view['display'];
  }

  public function setViewDisplay(string $id): static {
    $this->view['display'] = $id;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $this->addDependency('module', 'views');
    $this->addDependency('config', 'views.view.' . $this->getViewId());

    $parentId = $this->getParentId();
    if ($parentId) {
      $localTaskManager = \Drupal::getContainer()->get('plugin.manager.menu.local_task');
      try {
        $parentDefinition = $localTaskManager->getDefinition($parentId);
        if (!empty($parentDefinition['provider'])) {
          $this->addDependency('module', $parentDefinition['provider']);
        }
      }
      catch (PluginNotFoundException) {
        // Do nothing.
      }
    }

    return $this;
  }

}
