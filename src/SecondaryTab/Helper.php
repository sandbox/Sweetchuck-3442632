<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Menu\ContextualLinkManagerInterface;
use Drupal\Core\Menu\LocalActionManagerInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;

class Helper implements HelperInterface {

  public function __construct(
    protected CacheBackendInterface $cacheMenu,
    protected MenuLinkManagerInterface $menuLinkManager,
    protected ContextualLinkManagerInterface $contextualLinkManager,
    protected LocalTaskManagerInterface $localTaskManager,
    protected LocalActionManagerInterface $localActionManager,
  ) {
  }

  public function cacheRebuild(): static {
    $this->cacheMenu->invalidateAll();
    $this->menuLinkManager->rebuild();
    $this->contextualLinkManager->clearCachedDefinitions();
    $this->localTaskManager->clearCachedDefinitions();
    $this->localActionManager->clearCachedDefinitions();

    return $this;
  }

}
