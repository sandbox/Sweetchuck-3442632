<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

interface PermissionProviderInterface {

  public function getPermissions(): array;

  public function getBundlePermission(string $operation, string $bundleId = ''): string;

}
