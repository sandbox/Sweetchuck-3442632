<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @see \Drupal\Core\Entity\EntityDeleteForm
 */
class DeleteForm extends EntityConfirmFormBase {

  use EntityDeleteFormTrait;

  protected ConfigManagerInterface $configManager;

  protected function getConfigManager(): ConfigManagerInterface {
    return $this->configManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.manager'),
    );
  }

  public function __construct(ConfigManagerInterface $configManager) {
    $this->configManager = $configManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $entity = $this->getEntity();

    // Only do dependency processing for configuration entities. Whilst it is
    // possible for a configuration entity to be dependent on a content entity,
    // these dependencies are soft and content delete permissions are often
    // given to more users. This method should not make assumptions that $entity
    // is a configuration entity in case we decide to remove the following
    // condition.
    if (!($entity instanceof ConfigEntityInterface)) {
      return $form;
    }

    $this->addDependencyListsToForm(
      $form,
      $entity->getConfigDependencyKey(),
      $this->getConfigNamesToDelete($entity),
      $this->getConfigManager(),
      $this->entityTypeManager,
    );

    return $form;
  }

  /**
   * @return string[]
   */
  protected function getConfigNamesToDelete(ConfigEntityInterface $entity): array {
    return [
      $entity->getConfigDependencyName(),
    ];
  }

}
