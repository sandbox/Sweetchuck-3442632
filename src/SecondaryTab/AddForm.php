<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\views\Views;
use Drupal\views_st\Entity\ViewsSt;
use Drupal\views_st\ViewsStInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class AddForm extends EntityForm {

  use AutowireTrait;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\views_st\ViewsStInterface
   */
  protected $entity;

  public function __construct(
    #[Autowire(service: 'plugin.manager.menu.local_task')]
    protected LocalTaskManagerInterface $localTaskManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $entityType = $this->entity->getEntityType();
    $keys = $entityType->getKeys();

    $form[$keys['label']] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->entity->label(),
      '#description' => $this->t(
        'The human-readable name of this %entityType.singularLabel. This name must be unique.',
        [
          '%entityType.singularLabel' => $entityType->getSingularLabel(),
        ],
      ),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form[$keys['id']] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine-readable name'),
      '#default_value' => $this->entity->id(),
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => [ViewsSt::class, 'load'],
        'source' => [$keys['label']],
      ],
      '#description' => $this->t(
        'A unique machine-readable name for this %entityType.singularLabel. It must only contain lowercase letters, numbers, and underscores.',
        [
          '%entityType.singularLabel' => $entityType->getSingularLabel(),
        ],
      ),
    ];

    $form['parent'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('Parent "links.task"'),
      '#open' => TRUE,

      'id' => [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Identifier'),
        '#default_value' => $this->entity->getParentId(),
        '#autocomplete_route_name' => 'views_st.autocomplete.menu_local_task_primary',
      ],
    ];

    $form['weight'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Weight'),
      '#default_value' => $this->entity->getWeight(),
      '#step' => 1,
    ];

    $form['view'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => $this->t('View'),
      '#open' => TRUE,

      // Still easier to select View ID and Display ID concatenated and split
      // them later, than implement some kind of hierarchy between two
      // individual input elements.
      'id_and_display' => [
        '#type' => 'select',
        '#required' => TRUE,
        '#title' => $this->t('View display'),
        '#options' => $this->getViewAndDisplayOptions(),
        '#default_value' => $this->buildViewAndDisplayKey(
          $this->entity->getViewId(),
          $this->entity->getViewDisplay(),
        ),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    $allowedParentMenuLocalTasks = $this->getAllowedParentMenuLocalTasks();
    if (!array_key_exists($values['parent']['id'], $allowedParentMenuLocalTasks)) {
      $form_state->setError(
        $form['parent']['id'],
        $this->t('Parent local task identifier is invalid'),
      );
    }
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpMissingParentCallCommonInspection
   *   Unfortunately there is no separated API for "extract entity values from
   *   submitted values" and "set entity field values".
   *   The whole method has to be overwritten.
   */
  protected function copyFormValuesToEntity(
    EntityInterface $entity,
    array $form,
    FormStateInterface $form_state,
  ): void {
    /** @var \Drupal\views_st\ViewsStInterface $entity */
    $values = $this->convertFormValuesToEntityValues($entity, $form, $form_state);
    foreach ($values as $key => $value) {
      $entity->set($key, $value);
    }
  }

  protected function convertFormValuesToEntityValues(
    ViewsStInterface $entity,
    array $form,
    FormStateInterface $form_state,
  ): array {
    $values = $form_state->getValues();

    // Custom.
    $values['view'] = $this->splitViewAndDisplayKey($values['view']['id_and_display']) + $values['view'];
    unset($values['view']['id_and_display']);

    // Original.
    if ($this->entity instanceof EntityWithPluginCollectionInterface) {
      // Do not manually update values represented by plugin collections.
      $values = array_diff_key($values, $this->entity->getPluginCollections());
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $this
      ->saveSetMessage()
      ->saveSetRedirect($form_state);

    return $result;
  }

  /**
   * Place a status message after save.
   */
  protected function saveSetMessage(): static {
    $args = [
      '%label' => $this->entity->label(),
      '@type' => $this->entity->getEntityType()->getLabel(),
    ];

    $messenger = $this->messenger();
    switch ($this->getOperation()) {
      case 'add':
        $messenger->addStatus($this->t('"%label" @type has been created', $args));
        break;

      case 'edit':
        $messenger->addStatus($this->t('"%label" @type has been updated', $args));
        break;

    }

    return $this;
  }

  protected function saveSetRedirect(FormStateInterface $formState): static {
    $formState->setRedirect('entity.' . $this->entity->getEntityTypeId() . '.collection');

    return $this;
  }

  /**
   * @see \Drupal\views\Plugin\EntityReferenceSelection\ViewsSelection::buildConfigurationForm
   */
  protected function getViewAndDisplayOptions(): array {
    $options = [];
    foreach (Views::getApplicableViews('uses_route') as $view) {
      $key = $this->buildViewAndDisplayKey($view[0], $view[1]);
      $options[$key] = $key;
    }

    return $options;
  }

  protected function buildViewAndDisplayKey(string $viewId, string $displayId): string {
    return "$viewId:$displayId";
  }

  /**
   * @return array{id: string, display: string}
   */
  protected function splitViewAndDisplayKey(string $key): array {
    $parts = explode(':', $key);

    return [
      'id' => $parts[0],
      'display' => $parts[1],
    ];
  }

  protected function getAllowedParentMenuLocalTasks(): array {
    // When the "parent_id" is not empty, then it is already a secondary tab.
    // Sometimes the "id" is empty, those can't be used as parent.
    // Example for menu_local_task with empty "id":
    // - route_name = "devel.toolbar.settings_form".
    // - base_route = "devel.admin_settings".
    return array_filter(
      $this->localTaskManager->getDefinitions(),
      fn(array $definition): bool => empty($definition['parent_id']) && !empty($definition['id']),
    );
  }

}
