<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @property \Drupal\views_st\SecondaryTab\StorageInterface $storage
 */
class ListBuilder extends DraggableListBuilder {

  /**
   * @var \Drupal\views_st\ViewsStInterface[]
   */
  protected $entities = [];

  /**
   * {@inheritdoc}
   *
   * @return static
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    // @phpstan-ignore-next-line
    return new static(
      $entity_type,
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.menu.local_task'),
      $container->get('router.route_provider'),
    );
  }

  public function __construct(
    EntityTypeInterface $entity_type,
    MessengerInterface $messenger,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LocalTaskManagerInterface $localTaskManager,
    protected RouteProviderInterface $routeProvider,
  ) {
    $this->messenger = $messenger;

    /* @noinspection PhpUnhandledExceptionInspection */
    parent::__construct(
      $entity_type,
      $entityTypeManager->getStorage($entity_type->id()),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'views_st_views_st_list_form';
  }

  /**
   * @return array<string, \Drupal\views_st\ViewsStInterface[]>
   */
  protected function getEntitiesGroupedByParent(): array {
    $entities = [];
    foreach ($this->entities as $entity) {
      $entities[$entity->getParentId()][] = $entity;
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string, mixed>
   */
  public function buildHeader(): array {
    $header = [
      'id' => $this->t('ID'),
      'label' => $this->t('Label'),
      'view.id' => $this->t('View ID'),
      'view.display' => $this->t('View Display'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string, mixed>
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form[$this->entitiesKey] = [];

    $this->entities = $this->load();
    foreach ($this->getEntitiesGroupedByParent() as $parentId => $entities) {
      // @todo Add rows for foreign siblings and adjust the $delta.
      $delta = count($entities);

      $parentTask = NULL;
      $parentRoute = NULL;
      try {
        $parentTask = $this->localTaskManager->getDefinition($parentId);
        $parentRoute = $this->routeProvider->getRouteByName($parentTask['route_name']);
      }
      catch (\Exception) {
        // @todo Error handler.
      }

      $form[$this->entitiesKey][$parentId] = [
        '#type' => 'table',
        '#caption' => $this->t(
          '@local_task.parent.id (@local_task.parent.route.path)',
          [
            '@local_task.parent.id' => $parentId,
            '@local_task.parent.route.path' => $parentRoute->getPath(),
          ],
        ),
        '#header' => $this->buildHeader(),
        '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
        '#tabledrag' => [],
      ];

      $form[$this->entitiesKey][$parentId]['#tabledrag'][] = [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'weight',
      ];

      foreach ($entities as $entity) {
        $row = $this->buildRow($entity);

        if (isset($row['label'])) {
          $row['label'] = ['#plain_text' => $row['label']];
        }

        if (isset($row['weight'])) {
          $row['weight']['#delta'] = $delta;
        }

        $form[$this->entitiesKey][$parentId][$entity->id()] = $row;
      }
    }

    $isEmpty = empty($form[$this->entitiesKey]);
    if ($isEmpty) {
      $this->buildEmptyMessage($form, $form_state);
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
      '#access' => !$isEmpty,
    ];

    return $form;
  }

  /**
   * @phpstan-param array<string, mixed> $form
   */
  protected function buildEmptyMessage(array &$form, FormStateInterface $form_state): static {
    $form[$this->entitiesKey . ':empty'] = [
      '#prefix' => '<div class="message">',
      '#suffix' => '</div>',
      '#markup' => $this->t(
        'There is no any @label definition',
        [
          '@label' => $this->entityType->getLabel(),
        ],
      ),
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string, mixed>
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\views_st\ViewsStInterface $entity */
    $row = [
      '#parents' => [
        $this->entitiesKey,
        $entity->id(),
      ],
      '#attributes' => [
        'class' => ['draggable', 'tabledrag-leaf'],
      ],
      'id' => [
        'data' => [
          '#markup' => $entity->id(),
        ],
      ],
      'label' => $entity->label(),
      'viewId' => [
        'data' => [
          '#markup' => $entity->getViewId(),
        ],
      ],
      'viewDisplay' => [
        'data' => [
          // @todo Check access and Url::fromRoute()
          // route_name: entity.view.edit_display_form
          // path:       /admin/structure/views/view/{view}/edit/{display_id}.
          '#markup' => $entity->getViewDisplay(),
        ],
      ],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string, mixed>
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);

    if ($this->moduleHandler()->moduleExists('config')) {
      $operations['config_export'] = [
        'title' => $this->t('Export'),
        'weight' => 999,
        'url' => $this->ensureDestination(Url::fromRoute(
          'config.export_single',
          [
            'config_type' => $entity->getEntityTypeId(),
            'config_name' => $entity->id(),
          ],
        )),
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $rows = $form_state->getValue($this->entitiesKey);
    if (is_iterable($rows)) {
      parent::submitForm($form, $form_state);
    }

    $this->messenger->addStatus($this->t('The configuration options have been saved.'));
  }

}
