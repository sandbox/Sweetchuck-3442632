<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

interface HelperInterface {

  public function cacheRebuild(): static;

}
