<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

/**
 * @todo Inherit from \Sweetchuck\Utils\Comparer\ComparerBase.
 */
class DefaultComparer {

  protected int $result = 0;

  protected array $fieldNames = [
    'parent.id',
    'weight',
  ];

  public function getFieldNames(): array {
    return $this->fieldNames;
  }

  public function setFieldNames(array $fieldNames): static {
    $this->fieldNames = $fieldNames;

    return $this;
  }

  protected bool $ascending = TRUE;

  public function isAscending(): bool {
    return $this->ascending;
  }

  public function setAscending(bool $ascending): static {
    $this->ascending = $ascending;

    return $this;
  }

  public function __invoke($a, $b): int {
    return $this->compare($a, $b);
  }

  public function compare($a, $b): int {
    return $this
      ->initResult()
      ->setResult($a, $b)
      ->getResult();
  }

  protected function getResult(): int {
    if ($this->result === 0 || $this->isAscending()) {
      return $this->result;
    }

    return $this->result > 0 ? -1 : 1;
  }

  /**
   * @param \Drupal\views_st\ViewsStInterface $a
   * @param \Drupal\views_st\ViewsStInterface $b
   */
  protected function setResult($a, $b): static {
    foreach ($this->fieldNames as $fieldName) {
      $this->result = $a->get($fieldName) <=> $b->get($fieldName);

      if ($this->result !== 0) {
        return $this;
      }
    }

    return $this;
  }

  protected function initResult(): static {
    $this->result = 0;

    return $this;
  }

}
