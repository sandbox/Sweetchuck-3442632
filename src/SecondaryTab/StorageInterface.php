<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * @method \Drupal\views_st\ViewsStInterface      createFromStorageRecord(array $values)
 * @method null|\Drupal\views_st\ViewsStInterface load($id)
 * @method null|\Drupal\views_st\ViewsStInterface loadOverrideFree($id)($id)
 * @method \Drupal\views_st\ViewsStInterface[]    loadMultiple(array $ids = NULL)
 * @method \Drupal\views_st\ViewsStInterface[]    loadMultipleOverrideFree(array $ids = NULL)
 * @method \Drupal\views_st\ViewsStInterface[]    loadByProperties(array $values)
 */
interface StorageInterface extends ConfigEntityStorageInterface {
}
