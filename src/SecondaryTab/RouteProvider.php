<?php

declare(strict_types=1);

namespace Drupal\views_st\SecondaryTab;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

class RouteProvider extends AdminHtmlRouteProvider {
}
