<?php

declare(strict_types=1);

namespace Drupal\views_st\Controller;

use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * @phpstan-type MenuLocalTaskDefinition array{
 *   id: string,
 *   parent_id: ?string,
 *   route_name: string,
 * }
 * @phpstan-type FuzzyDocument array{
 *   menu_local_task__id: string,
 *   route__id: string,
 *   route__path: string,
 * }
 * @phpstan-type AutocompleteItem array{
 *   value: string,
 *   label: string,
 * }
 */
class AutocompleteMenuLocalTasksPrimaryController extends AutocompleteBase {

  public function __construct(
    #[Autowire(service: 'plugin.manager.menu.local_task')]
    protected LocalTaskManagerInterface $localTaskManager,
    #[Autowire(service: 'router.route_provider')]
    protected RouteProviderInterface $routeProvider,
  ) {
  }

  /**
   * @return array<string, mixed>
   */
  protected function getFuzzyOptions(): array {
    $options = parent::getFuzzyOptions();
    $options['keys'][] = 'menu_local_task__id';
    $options['keys'][] = 'route__id';
    $options['keys'][] = 'route__path';

    return $options;
  }

  /**
   * @phpstan-return array<MenuLocalTaskDefinition>
   */
  protected function getMenuLocalTasksPrimary(): array {
    // When the "parent_id" is not empty, then it is already a secondary tab.
    // Sometimes the "id" is empty, those can't be used as parent.
    // Example for menu_local_task with empty "id":
    // - route_name = "devel.toolbar.settings_form".
    // - base_route = "devel.admin_settings".
    return array_filter(
      $this->localTaskManager->getDefinitions(),
      fn(array $definition): bool => empty($definition['parent_id']) && !empty($definition['id']),
    );
  }

  /**
   * @phpstan-return array<array{
   *   menu_local_task: MenuLocalTaskDefinition,
   * }>
   */
  protected function getFuzzyDocuments(string $keyword): array {
    $documents = [];
    foreach ($this->getMenuLocalTasksPrimary() as $menuLocalTaskDefinition) {
      $documents[] = $this->convertEntryToFuzzyDocument([
        'menu_local_task' => $menuLocalTaskDefinition,
      ]);
    }

    return $documents;
  }

  /**
   * @phpstan-param array{menu_local_task: array<string, mixed>} $entry
   *
   * @phpstan-return FuzzyDocument
   */
  protected function convertEntryToFuzzyDocument(array $entry): array {
    $route = $this->routeProvider->getRouteByName($entry['menu_local_task']['route_name']);

    return [
      'menu_local_task__id' => $entry['menu_local_task']['id'],
      'route__id' => $entry['menu_local_task']['route_name'],
      'route__path' => $route->getPath(),
    ];
  }

  /**
   * @phpstan-param array{item: FuzzyDocument} $result
   *
   * @phpstan-return AutocompleteItem
   */
  protected function convertFuzzyResultToAutocompleteItem(array $result): array {
    return [
      'value' => $result['item']['menu_local_task__id'],
      'label' => "{$result['item']['menu_local_task__id']} ({$result['item']['route__path']})",
    ];
  }

}
