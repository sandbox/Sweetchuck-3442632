<?php

declare(strict_types=1);

namespace Drupal\views_st\Controller;

use Drupal\Core\Controller\ControllerBase;
use Fuse\Fuse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AutocompleteBase extends ControllerBase {

  protected int $minKeywordLength = 3;

  /**
   * @abstract
   */
  protected function getFuzzyOptions(): array {
    return [
      'keys' => [],
      'threshold' => 0.5,
      'includeScore' => TRUE,
      'shouldSort' => TRUE,
      'limit' => 20,
    ];
  }

  public function body(Request $request) {
    $keyword = mb_strtolower($request->query->get('q', ''));
    $items = mb_strlen($keyword) >= $this->minKeywordLength ?
      $this->getAutocompleteItems($keyword)
      : [];

    return new JsonResponse($items);
  }

  protected function getAutocompleteItems(string $keyword): array {
    $fuse = new Fuse(
      $this->getFuzzyDocuments($keyword),
      $this->getFuzzyOptions(),
    );
    $items = [];
    foreach ($fuse->search($keyword) as $result) {
      $items[] = $this->convertFuzzyResultToAutocompleteItem($result);
    }

    return $items;
  }

  abstract protected function getFuzzyDocuments(string $keyword): array;

  abstract protected function convertEntryToFuzzyDocument(array $entry): array;

  abstract protected function convertFuzzyResultToAutocompleteItem(array $result): array;

}
