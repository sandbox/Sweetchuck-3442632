<?php

declare(strict_types=1);

namespace Drupal\views_st\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LinksTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id,
  ) {
    // @phpstan-ignore-next-line
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('state'),
    );
  }

  public function __construct(
    protected string $basePluginId,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected StateInterface $state,
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $base_plugin_definition
   *
   * @phpstan-return array<string, mixed>
   *
   * @noinspection PhpMissingParentCallCommonInspection
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    try {
      /** @var \Drupal\views_st\SecondaryTab\StorageInterface $viewsStStorage */
      $viewsStStorage = $this->entityTypeManager->getStorage('views_st');

      /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $viewStorage */
      $viewStorage = $this->entityTypeManager->getStorage('view');
    }
    catch (PluginNotFoundException | InvalidPluginDefinitionException) {
      return $this->derivatives;
    }

    $secondaryTabs = $viewsStStorage->loadByProperties(['status' => TRUE]);
    if (!$secondaryTabs) {
      return $this->derivatives;
    }

    $existingRoutes = $this->state->get('views.view_route_names');
    if (!$existingRoutes) {
      return $this->derivatives;
    }

    foreach ($secondaryTabs as $secondaryTab) {
      /** @var \Drupal\views\Entity\View $view */
      $view = $viewStorage->load($secondaryTab->getViewId());
      if (!$view) {
        continue;
      }

      $viewExecutable = $view->getExecutable();
      $viewExecutable->initDisplay();
      if (!$viewExecutable->displayHandlers->has($secondaryTab->getViewDisplay())) {
        continue;
      }

      $viewRouteName = $existingRoutes[$viewExecutable->storage->id() . '.' . $secondaryTab->getViewDisplay()] ?? NULL;
      if (!$viewRouteName) {
        continue;
      }

      // @todo Check if the ViewDisplay uses required arguments.
      $this->derivatives[$secondaryTab->id()] = [
        'route_name' => $viewRouteName,
        'parent_id' => $secondaryTab->getParentId(),
        'weight' => $secondaryTab->getWeight(),
        'title' => $viewExecutable->getTitle(),
      ] + $base_plugin_definition;
      unset($this->derivatives[$secondaryTab->id()]['deriver']);
    }

    return $this->derivatives;
  }

}
